<?php

namespace Tests\Feature;

use App\User;
use Tests\TestCase;
use App\Model\Company;
use Illuminate\Support\Str;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CompanyEmployeeTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    function company_can_have_an_employee ()
    {
        $this->withExceptionHandling();

        $company = factory(Company::class)->create();
        $newUser = factory(User::class)->create();

        $this->signIn($company->owner);

        $invited = $this->post("/dashboard{$company->path()}/invite", [
            'email' => $newUser->email
        ]);
        

        $invited->assertStatus(200)
        ->assertJsonFragment([
            'success' => 'User has been added.'
        ]);

        $this->assertDatabaseHas('employees', [
            'user_id' => $newUser->id,
            'company_id' => $company->id
        ]);
    }

    // /** @test */
    // function company_cannot_add_company_owner ()
    // {
    //     $this->withExceptionHandling();

    //     $company = factory(Company::class)->create();
 
    //     $this->signIn($company->owner);

    //     $invited = $this->post("{$company->path()}/invite", [
    //         'email' => $company->owner->email
    //     ]);

    //     $invited->assertStatus(403)
    //     ->assertJsonFragment([
    //         'error' => 'You own this company.'
    //     ]);
    // }


    // /** @test */
    // function company_cannot_add_same_employee_twice ()
    // {
    //     $this->withExceptionHandling();

    //     $company = factory(Company::class)->create();
    //     $newUser = factory(User::class)->create();
 
    //     $this->signIn($company->owner);
    //     $company->employees()->attach($newUser, ['identifier' => Str::uuid()]);

    //     $invited = $this->post("{$company->path()}/invite", [
    //         'email' => $newUser->email
    //     ]);

    //     $invited->assertStatus(403)
    //     ->assertJsonFragment([
    //         'error' => 'User is already in Company.'
    //     ]);

    // }

    // /** @test */
    // function company_can_remove_an_employee ()
    // {
    //     $this->withExceptionHandling();

    //     $company = factory(Company::class)->create();
    //     $newUser = factory(User::class)->create();
 
    //     $this->signIn($company->owner);

    //     $company->invite($newUser, ['identifier' => Str::uuid()]);

    //     $invited = $this->post("{$company->path()}/detach", [
    //         'email' => $newUser->email
    //     ]);

    //     $invited->assertStatus(200)
    //     ->assertJsonFragment([
    //         'success' => 'User has been removed from your company.'
    //     ]);

    //     $this->assertTrue(!$company->employees->contains($newUser));
    // }

}
