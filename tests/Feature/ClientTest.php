<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Model\Client;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ClientTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    function company_owner_can_add_client ()
    {
        $company = $this->signInWithCompany();

        $this->post("{$company->path()}/client", $attr = [
            'name' => 'new name',
            'email' => 'new@email.com',
            'address' => 'earth',
            'birthday' => '25-05-1990',
        ])->assertStatus(200)
        ->assertJsonFragment(['success' => 'User has been added.']);

        $this->assertDatabaseHas('clients', [
            'name' => 'new name',
            'email' => 'new@email.com',
            'address' => 'earth',
            'birthday' => '1990-05-25 00:00:00',
            'company_id' => $company->id
        ]);
    }

    /** @test */
    function client_can_be_deleted () 
    {
        $this->withExceptionHandling();

        $company = $this->signInWithCompany();

        $client = $company->inviteClient([
            'name' => 'new name',
            'email' => 'new@email.com',
            'address' => 'earth',
            'birthday' => '25-05-1990'
        ]);

        $this->delete($client->path())
        ->assertStatus(200);

        $this->assertDatabaseMissing('clients', [
            'id' => $client->id
        ]);
    }
}
