<?php

namespace Tests\Feature;

use App\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CompanyTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    function a_user_can_make_a_company ()
    {
        $this->withExceptionHandling();
        $this->signIn();

        $create = $this->post('/dashboard/company', $attr = [
            'name' => 'New Company'
        ]);

        $create->assertStatus(200)
            ->assertJsonFragment([
                'success' => 'Your company has been created.'
            ]);

        $this->assertDatabaseHas('companies', $attr);
    }


    /** @test */
    function a_user_cannot_make_a_company_without_name ()
    {
        $this->withExceptionHandling();

        $this->signIn();

        $create = $this->post('/dashboard/company', $attr = [
            'name' => ''
        ]);

        $create->assertStatus(302)
            ->assertSessionHasErrors('name');
    }
}
