<?php

namespace Tests\Feature;

use App\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class LoanTest extends TestCase
{

    use RefreshDatabase;


    /** @test */
    public function client_can_have_loan()
    {
        $company = $this->signInWithCompany();

        $client = $company->inviteClient([
            'name' => 'new name',
            'email' => 'new@email.com',
            'address' => 'earth',
            'birthday' => '25-05-1990'
        ]);

        $loan = $this->post($client->path() .'/loan/create', $attr = [
            'amount' => 6000,
            'interest' => 3,
            'term' => 4,
            'mode' => 2, // 2 = per cut off, 1 = per month
        ]);

        $this->assertDatabaseHas('loans', [
            'amount' => 6000,
            'interest' => 3,
            'term' => 4,
            'mode' => 2,
            'balance' => 6720,
            'total_payment' => 0
        ]);
    }


    /** @test */
    public function client_cannot_be_created_without_amount()
    {
        $company = $this->signInWithCompany();

        $client = $company->inviteClient([
            'name' => 'new name',
            'email' => 'new@email.com',
            'address' => 'earth',
            'birthday' => '25-05-1990'
        ]);

        $loan = $this->post($client->path() .'/loan/create', $attr = [
            'amount' => '',
            'interest' => 3,
            'term' => 4,
            'mode' => 1, // 2 = per cut off, 1 = per month
        ]);

        $loan->assertStatus(302);
        $loan->assertSessionHasErrors('amount');
    }


    /** @test */
    public function loan_can_be_updated()
    {
        $company = $this->signInWithCompany();

        $client = $company->inviteClient([
            'name' => 'new name',
            'email' => 'new@email.com',
            'address' => 'earth',
            'birthday' => '25-05-1990'
        ]);

        $loan = $client->createLoan([
            'amount' => 6000,
            'interest' => 3,
            'term' => 4,
            'mode' => 2, // 2 = per cut off, 2 = per month
        ]);


        $this->patch($loan->path(), $attr = [
            'amount' => 5000,
            'interest' => 3,
            'term' => 5,
            'mode' => 2   
        ]);

        $this->assertDatabaseHas('loans', [
            'amount' => 5000,
            'interest' => 3,
            'term' => 5,
            'mode' => 2,
            'balance' => 5750,
            'total_payment' => 0
        ]);
    }
}
