<?php

namespace Tests\Feature;

use App\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class PaymentTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function loan_can_be_paid()
    {
        $this->withExceptionHandling();
        $company = $this->signInWithCompany();

        $client = $company->inviteClient(factory(User::class)->create());

        $loan = $client->createLoan([
            'amount' => 6000,
            'interest' => 3,
            'term' => 4,
            'mode' => 2, // 2 = per cut off, 1 = per month
        ]);

        $pay = $this->post($loan->path() . '/payment', $attr = [
            'amount' => 840
        ]);

        $this->assertDatabaseHas('payments', [
            'amount' => 840,
            'principal' => 750,
            'interest' => 90
        ]);


        $this->assertDatabaseHas('loans', [
            'amount' => 6000,
            'interest' => 3,
            'term' => 4,
            'mode' => 2,
            'balance' => 5880,
            'total_payment' => 840,
        ]);
    }
}
