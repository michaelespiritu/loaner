<?php

namespace Tests;

use Illuminate\Foundation\Testing\TestCase as BaseTestCase;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;

    protected function signIn($user = null)
    {
        $user = $user ?: factory('App\User')->create();
        $this->actingAs($user);
        return $user;
    }


    protected function signInWithCompany($company = null)
    {
        $company = $company ?: factory('App\Model\Company')->create();
        $this->actingAs($company->owner);
        return $company;
    }
}
