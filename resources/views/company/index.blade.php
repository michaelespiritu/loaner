@extends('layouts.app')

@section('content')
<div id="app">
    <div class="container">
        <create-company></create-company>
        &nbsp;
        <invite-user :company="{{json_encode(auth()->user()->companyProfile())}}"></invite-user>
    </div>
</div>
@endsection

@section('script')

<!-- Scripts -->
<script src="{{ asset('js/app.js') }}" defer></script>

@endsection
