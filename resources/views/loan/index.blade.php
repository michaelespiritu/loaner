@extends('layouts.two-col')

@section('sidebar')
    @include('loan.sidebar')
@endsection

@section('content')
<div id="app">
    <div class="container">
        <all-loan 
            :client="{{json_encode($client)}}"
            :loans="{{json_encode($loans)}}"
            ></all-loan>
    </div>
</div>
@endsection

@section('script')

<!-- Scripts -->
<script src="{{ asset('js/app.js') }}" async></script>

@endsection
