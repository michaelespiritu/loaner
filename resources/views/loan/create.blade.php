@extends('layouts.two-col')

@section('sidebar')
    @include('loan.sidebar')
@endsection

@section('content')
<div id="app">
    <div class="container">
        <create-loan :client="{{json_encode($client)}}"></create-loan>
    </div>
</div>
@endsection

@section('script')

<!-- Scripts -->
<script src="{{ asset('js/app.js') }}" defer></script>

@endsection
