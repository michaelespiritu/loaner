<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <title>{{$client->name}}</title>
        <style type="text/css" media="all">

            @page {
                margin: 0cm 0cm;
            }

            /** Define now the real margins of every page in the PDF **/
            body {
                margin-top: 2cm;
                margin-left: 1cm;
                margin-right: 1cm;
                margin-bottom: 2cm;
            }

            * {
                font-family: 'Raleway';
                font-weight: 400;
            }

            h1, h2, h3, h4, h5, h6 {
                margin-top: 0px;
                padding-top: 0px;
                padding-bottom: 10px;
                line-height: 1.5;
                font-family: 'Raleway', sans-serif;
            }

            p {
                font-size: 15pt;
                margin-top: 0px;
                padding-top: 0px;
                padding-bottom: 10px;
                line-height: 1.5Th;
                font-family: 'Raleway', sans-serif;
            }
            
            .sb-0 {
                margin-bottom: 0px;
                padding-bottom: 0px;
            }

            u {
                font-family: 'Raleway', sans-serif;
            }


            strong {
                font-family: 'Raleway', sans-serif;
                font-weight: bold;
            }

            .content {
                margin: 25px 35px;;
            }

            .content .body {
                line-height: 1.2 !important;
            }

            .content .body p {
                margin-bottom: 5px;
            }

            .text-center {
                text-align: center;
            }

            .top {
                position: fixed;
                top: 0cm;
                left: 0cm;
                right: 0cm;
                height: 0.6cm;

                padding: 15px 0px;
                text-align: center;
                background: #346cb0;
                color: #fff;
                width: 100%;
            }

            .bottom {
                position: fixed; 
                bottom: 0cm; 
                left: 0cm; 
                right: 0cm;
                height: 0.6cm;

                padding: 15px 0px;
                margin-top: 10px;
                text-align: center;
                background: #346cb0;
                color: #fff;
                width: 100%;
            }

             
        </style>
    </head>
    <body>
        <header class="top">
            
        </header>

        <footer class="bottom">
            
        </footer>

        <main class="content">
            <div class="body">
                <h1 class="text-center">TERMS OF LOAN AGREEMENT</h1>
                <br />
                <br />
                <p>I, <u>{{ ucfirst($client->name) }}</u>, borrowed PHP {{ number_format($loan->amount, 2, '.', ',') }} from <u>{{ $client->company->owner->name }}</u> on {{ $loan->created_at->format('F d, Y') }}. </p>
                
                <p><u>{{$client->company->owner->name}}</u> and I both agree that the loan will be repaid using a series of scheduled financial payments.</p>

                @if($loan->mode == 1)
                <p>I, <u>{{ ucfirst($client->name) }}</u>, will submit a monthly payment due on the 15th of every month in the amount of PHP {{ number_format($loan->calculateMonthly(), 2, '.', ',') }} to <u>{{ ucfirst($client->company->owner->name) }}</u>.</p> 
                @else
                <p>I, <u>{{ucfirst($client->name)}}</u>, will submit a monthly payment due on the 15th and 30th of every month in the amount of PHP {{ number_format($loan->calculateMonthly(), 2, '.', ',') }} to <u>{{ ucfirst($client->company->owner->name) }}</u>.</p> 
                @endif
                
                <p>The first payment will be made on <u>{{ $loan->created_at->addDays(30)->format('F d, Y') }}</u> and the last payment will be made on <u>{{ $loan->created_at->addMonths($loan->term)->format('F d, Y') }}<u>.</p>

                <p>I, <u>{{ ucfirst($client->name) }}</u>, agree to a <strong>{{ $loan->interest }}%</strong> interest rate per month for a period of  <strong>{{ $loan->term }} months</strong>.</p>

                <p>I, <u>{{ ucfirst($client->name) }}</u>, agree to a PHP 15.00 late charge per day for any late payments after the promised payment duration, until the entire loan is paid in full.</p>

                <br />
                <br />
                <br />
                <br />
                <br />

                <!-- <div style="width: 100%;display: flex; align-items: center; justify-content: spaced-between"> -->
                <p style="float: left">
                    <br>
                    <strong>{{ ucfirst($client->name) }}</strong>
                </p>


                <p style="float: right">
                    <img src="{{ public_path('sig.png') }}" style="width: 150px; margin: 0 auto; text-align: center;"/><br>
                    <strong>{{ ucfirst($client->company->owner->name) }}</strong>
                </p>
                <!-- </div> -->
            </div>
        </main>
        
    </body>
</html>