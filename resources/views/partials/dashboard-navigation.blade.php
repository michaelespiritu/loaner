<div class="navigation navbar-light bg-white shadow-sm py-2 position-relative" id="dashboard-nav">
    <ul class="nav navbar-light container px-0">
        <li class="nav-item">
            <a class="nav-link active" href="/dashboard/client">Clients</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="#">Link</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="#">Link</a>
        </li>
        <li class="nav-item">
            <a class="nav-link disabled" href="#" tabindex="-1" aria-disabled="true">Disabled</a>
        </li>
    </ul>
</div>