@include('partials.header')
    <div>
        
        @include('partials.navigation')
        @include('partials.dashboard-navigation')
        
        <main class="py-4">
            @yield('content')
        </main>

    </div>

@include('partials.footer')

    
