@include('partials.header')
    <div id="body-app">
        
        @include('partials.navigation')
        @include('partials.dashboard-navigation')

        <div class="container-fluid">
            <div class="row">
                <nav class="col-md-2 d-none d-md-block bg-light sidebar">
                    <div class="sidebar-sticky">
                        @yield('sidebar')
                    </div>
                </nav>

                <main class="col-md-10 ml-sm-auto col-lg-10 px-0">
                    <div class="px-4 py-4">
                        @yield('content')
                    </div>

                </main>
            </div>
        </div>
        

    </div>

@include('partials.footer')

    
