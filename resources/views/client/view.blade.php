@extends('layouts.two-col')

@section('sidebar')
    @include('client.sidebar')
@endsection

@section('content')
<div id="app">
    <div class="container">
        <div class="card">
            <div class="card-body">
                <h1>{{$client->name}}</h1>
                <p>{{$client->email}}</p>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')

<!-- Scripts -->
<script src="{{ asset('js/app.js') }}" defer></script>

@endsection
