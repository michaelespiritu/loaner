@extends('layouts.two-col')

@section('sidebar')
    @include('client.sidebar')
@endsection

@section('content')
<div id="app">
    <div class="container">
        <invite-client :company="{{json_encode(auth()->user()->companyProfile())}}"></invite-client>
    </div>
</div>
@endsection

@section('script')

<!-- Scripts -->
<script src="{{ asset('js/app.js') }}" defer></script>

@endsection
