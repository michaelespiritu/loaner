@extends('layouts.two-col')

@section('sidebar')
    @include('client.sidebar')
@endsection

@section('content')
<div id="app">
    <div class="container">
        <all-client 
            :company="{{json_encode($company)}}"
            :clients="{{json_encode($clients)}}"></all-client>
    </div>
</div>
@endsection

@section('script')

<!-- Scripts -->
<script src="{{ asset('js/app.js') }}" defer></script>

@endsection
