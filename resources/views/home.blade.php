@extends('layouts.app')

@section('content')
<!-- <div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    You are logged in!
                </div>
            </div>
        </div>
    </div>
</div> -->
<div id="app">
    <div class="container">
        <create-company></create-company>
        &nbsp;
        <invite-user :company="{{json_encode(auth()->user()->companyProfile())}}"></invite-user>
    </div>
</div>
@endsection


@section('script')

<!-- Scripts -->
<script src="{{ asset('js/app.js') }}" defer></script>

@endsection
