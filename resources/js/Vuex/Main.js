import Vue from 'vue';
import Vuex from 'vuex';

import Client from './Web/Client';
import Loan from './Web/Loan';
import Company from './Web/Company';

Vue.use(Vuex);
Vue.config.debug = true;

const debug = process.env.NODE_ENV !== 'production';

export default new Vuex.Store({
    modules: {
        Client,
        Loan,
        Company
    },
    strict: false
});
