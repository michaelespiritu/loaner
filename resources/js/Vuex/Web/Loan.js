const getDefaultState = () => {
    return {
        All: [],
        View: []
    }
  }
  
  const state = getDefaultState()
  
  
  const mutations = {
        SET_ALL_LOAN(state, data) {
            state.All = data;
        },
        RESET_ALL_LOAN(state, data) {
            state.All = [];
        },
        SET_VIEW_LOAN(state, data) {
            state.View = data;
        },
        RESET_VIEW_LOAN(state, data) {
            state.View = [];
        }
  }
  
  
  export default {
      state,
      mutations
  }
  