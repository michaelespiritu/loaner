const getDefaultState = () => {
    return {
        Data: []
    }
  }
  
  const state = getDefaultState()
  
  
  const mutations = {
        SET_COMPANY_DATA(state, data) {
            state.Data = data;
        },
        RESET_COMPANY_DATA(state, data) {
            state.Data = [];
        }
  }
  
  
  export default {
      state,
      mutations
  }
  