const getDefaultState = () => {
    return {
        All: [],
        View: []
    }
  }
  
  const state = getDefaultState()
  
  
  const mutations = {
        SET_ALL_CLIENT(state, data) {
            state.All = data;
        },
        RESET_ALL_CLIENT(state, data) {
            state.All = [];
        },
        SET_VIEW_CLIENT(state, data) {
            state.View = data;
        },
        RESET_VIEW_CLIENT(state, data) {
            state.View = [];
        }
  }
  
  
  export default {
      state,
      mutations
  }
  