require('./bootstrap');

Vue.component('create-company', require('./components/Company/Create.vue').default);
Vue.component('invite-user', require('./components/Employee/Invite.vue').default);
Vue.component('invite-client', require('./components/Client/Invite.vue').default);
Vue.component('all-client', require('./components/Client/All.vue').default);

Vue.component('create-loan', require('./components/Loan/Create.vue').default);
Vue.component('all-loan', require('./components/Loan/All.vue').default);

import Store from './Vuex/Main'

const app = new Vue({
    el: '#app',
    store: Store
});
