<?php

namespace App\Http\Controllers\Web;

use App\User;
use App\Model\Company;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CompanyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('company.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate(['name' => 'required']);

        auth()->user()->company()->create([
            'identifier' => Str::uuid(),
            'name' => request()->name
        ]);

        return response()->json([
            'success' => 'Your company has been created.',
        ], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function show(Company $company)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function edit(Company $company)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Company $company)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function destroy(Company $company)
    {
        //
    }

    public function invite (Company $company)
    {   
        $user = User::whereEmail(request('email'))->first();

        $company->inviteEmployee($user);

        return response()->json([
            'success' => 'User has been added.',
        ], 200);
    }


    public function detach (Company $company)
    {   
        $user = User::whereEmail(request('email'))->first();

        $attach = $company->detach($user);

        return response()->json([
            'success' => 'User has been removed from your company.',
        ], 200);
    }
}
