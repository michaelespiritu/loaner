<?php

namespace App\Http\Controllers\Web;

use Carbon\Carbon;
use App\Model\Client;
use App\Model\Company;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\ClientResource;
use App\Http\Resources\CompanyResource;

class ClientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $clients = ClientResource::collection(auth()->user()->company->clients);
        $company = CompanyResource::make(auth()->user()->company);
        return view('client.index', compact('clients', 'company'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('client.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Company $company)
    {

        $company->inviteClient(array_merge(request()->all()));

        return response()->json([
            'success' => 'Client has been added.',
            'client' => ClientResource::collection(auth()->user()->company->clients)
        ], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Client $client)
    {
        return view('client.view', compact('client'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Client $client)
    {
        $birthday = [
            'birthday' => Carbon::parse(request()->birthday)
        ];

        $client->update(array_merge(request()->except('birthday'), $birthday));

        return response()->json([
            'success' => 'Client has been updated.',
            'client' => ClientResource::collection(auth()->user()->company->clients)
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Client $client)
    {
        $client->delete();

        return response()->json([
            'success' => 'Client has been deleted.',
            'client' => ClientResource::collection(auth()->user()->company->clients)
        ], 200);
    }
}
