<?php

namespace App\Http\Controllers\Web;

use PDF;
use App\Model\Loan;
use App\Model\Client;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\LoanResource;
use App\Http\Resources\ClientResource;

class LoanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Client $client)
    {
        return view('loan.index')
            ->with('client', ClientResource::make($client))
            ->with('loans', LoanResource::collection($client->loans));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Client $client)
    {
        

        $client = ClientResource::make($client);
        return view('loan.create', compact('client'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Client $client)
    {
        request()->validate([
            'amount' => 'required'
        ]);
        
        

        $client->createLoan(request()->all());

        return response()->json([
            'success' => 'Loan has been created.',
            'loans' => LoanResource::collection($client->loans->fresh())
        ], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Loan $loan)
    {
        $interest = request()->interest / 100 * request()->amount;
        $totalInterest = $interest * request()->term;
        $total = $totalInterest + request()->amount;

        $originalRemaining = ($loan->mode * $loan->term) - $loan->remaining_term;

        $loan->update([
            'amount' => ($loan->paymentCount() != 0 ) ? $loan->amount : request()->amount,
            'interest' => request()->interest,
            'term' => request()->term,
            'mode' => request()->mode,
            'balance' => $total - $loan->total_payment,
            'remaining_term' => (request()->mode * request()->term) - $originalRemaining
        ]);

        return response()->json([
            'success' => 'Loan has been created.',
            'loans' => LoanResource::collection($loan->client->loans->fresh())
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function termsAndCondition(Client $client, Loan $loan)
    {
        $pdf = PDF::loadView('loan.print', array('client' => $client, 'loan' => $loan));

        if (request()->v == 'stream') {
            return $pdf->stream($client->name.'.pdf');
        }

        return $pdf->download($client->name.'.pdf');
    }


    public function payment(Loan $loan)
    {
        $loan->createPayment(request()->amount);

        return response()->json([
            'success' => 'Payment has been made.',
            'loans' => LoanResource::collection($loan->client->loans->fresh())
        ], 200);
    }
}
