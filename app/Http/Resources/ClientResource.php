<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ClientResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'name' => $this->name,

            'email' => $this->email,

            'address' => $this->address,

            'birthday' => $this->birthday->format('F d, Y'),

            'birthday_view' => $this->birthday->format('d-m-Y'),

            'identifier' => $this->identifier,

            'open_loan' => $this->openLoan(),

            'closed_loan' => $this->closedLoan(),
            
            'path' => $this->path()
        ];
    }
}
