<?php

namespace App\Http\Resources;

use Carbon\Carbon;
use App\Http\Resources\PaymentResource;
use Illuminate\Http\Resources\Json\JsonResource;

class LoanResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'identifier' => $this->identifier,

            'amount' => number_format($this->amount, 2, '.', ','),

            'amount_raw' => $this->amount,

            'interest' => $this->interest,

            'total_interest_per_month' => number_format($this->totalAmountInterest(), 2, '.', ','),

            'total_interest' => number_format($this->totalAmountInterestAll(), 2, '.', ','),

            'total_amount_payable' => number_format($this->totalAmountToPay(), 2, '.', ','),

            'mode' => $this->mode,

            'mode_text' => ( $this->mode == 2) ? 'Per Cut Off' : 'Per Month',

            'term_raw' => $this->term,

            'term' => ($this->term > 1) ? $this->term .' months' : $this->term .' month',

            'monthly_amount' =>  ($this->mode == 1) ? number_format($this->calculateMonthly(), 2, '.', ',') . ' per month' : number_format($this->calculateMonthly(), 2, '.', ',') . ' per cut off',

            'total_payment' => number_format($this->total_payment, 2, '.', ','),

            'balance' => number_format($this->balance, 2, '.', ','),

            'remaining_term' => $this->remaining_term,
            
            'path' => $this->path(),

            'payment_history' => PaymentResource::collection($this->payments),

            'payment_count' => $this->paymentCount(),

        ];
    }
}
