<?php

namespace App\Model;

use App\Model\Client;
use App\Model\Payment;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;

class Loan extends Model
{
    protected $fillable = [
        'identifier', 'client_id', 'amount', 'interest', 'term', 'mode', 'balance', 'total_payment', 'remaining_term'
    ];

    /**
     * Get the route key for the model.
     *
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'identifier';
    }

    public function client()
    {
        return $this->belongsTo(Client::class);
    }


    public function totalAmountInterest()
    {
        return $this->interest / 100 * $this->amount;
    }

    public function totalAmountInterestAll()
    {
        $interest = $this->interest / 100 * $this->amount;
        return $interest * $this->term;
    }


    public function totalAmountToPay()
    {
        $interest = $this->interest / 100 * $this->amount;
        $totalInterest = $interest * $this->term;
        return $totalInterest + $this->amount;
    }


    public function calculateMonthly()
    {
        return $this->balance / $this->remaining_term;
    }


    public function calculateBalance()
    {
        $interest = $this->interest / 100 * $this->amount;
        $totalInterest = $interest * $this->term;
        $totalAmount = $totalInterest + $this->amount;

        $totalTerm = $this->mode * $this->term;

        return $totalAmount / $totalTerm;
    }


    public function calculatePayment($payment)
    {
        $tInterest = $this->interest * $this->term;
        $principal = ($payment * 100) / ($tInterest + 100);

        $interest = $payment - $principal;

        return [
            'amount' => $payment,
            'interest' => $interest,
            'principal' => $principal
        ];
    }


    public function payments ()
    {
        return $this->hasMany(Payment::class);
    }


    public function paymentCount ()
    {
        return count($this->payments);   
    }


    public function createPayment ($payment)
    {
        $insert = [
            'identifier' => Str::uuid(),
        ];

        $this->update([
            'balance' => $this->balance - $payment,
            'total_payment' => $this->total_payment + $payment,
            'remaining_term' => $this->remaining_term - 1
        ]);
        
        return $this->payments()->create(array_merge($insert, $this->calculatePayment($payment)));
    }

    public function path()
    {
        return "/dashboard/loan/$this->identifier";
    }
}
