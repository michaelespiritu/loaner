<?php

namespace App\Model;

use App\User;
use Carbon\Carbon;
use App\Model\Client;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    protected $fillable = [
        'identifier', 'name', 'company_image'
    ];

    /**
     * Get the route key for the model.
     *
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'identifier';
    }

    public function owner()
    {
        return $this->belongsTo(User::class, 'owner_id');
    }

    public function employees()
    {
        return $this->hasMany(Employee::class);
    }


    public function inviteEmployee($user)
    {
        return $this->employees()->create([
            'user_id' => $user->id,
            'identifier' => Str::uuid()
        ]);
        // return $this->employees()->attach($user, ['identifier' => Str::uuid()]);
    }

    // public function detach($user)
    // {
    //     return $this->employees()->detach($user);
    // }

    public function clients()
    {
        return $this->hasMany(Client::class, 'company_id');
    }

    public function inviteClient($data)
    {
        $insert = [
            'identifier' => Str::uuid(),
            'birthday' => Carbon::parse($data['birthday'])
        ];

        return $this->clients()->create(array_merge((array) $data, (array) $insert));
    }

    public function path ()
    {
        return "/dashboard/company/$this->identifier";
    }
}
