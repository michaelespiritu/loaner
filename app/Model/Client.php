<?php

namespace App\Model;

use App\User;
use App\Model\Company;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    protected $fillable = [
        'identifier', 'name', 'email', 'birthday', 'address', 'company_id'
    ];

    protected $dates = [
        'birthday',
    ];

    /**
     * Get the route key for the model.
     *
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'identifier';
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function company()
    {
        return $this->belongsTo(Company::class, 'company_id');
    }


    public function loans()
    {
        return $this->hasMany(Loan::class);
    }


    public function openLoan()
    {
        return count($this->loans()->where('balance', '!=' ,0)->get());
    }


    public function closedLoan()
    {
        return count($this->loans()->where('balance',  0)->get());
    }

    public function createLoan($data)
    {
        $interest = $data['interest'] / 100 * $data['amount'];
        $totalInterest = $interest * $data['term'];
        $total = $totalInterest + $data['amount'];

        $insert = [
            'identifier' => Str::uuid(),
            'balance' => $total,
            'remaining_term' => $data['mode'] * $data['term']
        ];

        return $this->loans()->create(array_merge((array) $insert, (array) $data));
    }


    public function updateLoan($data)
    {
        $interest = $data['interest'] / 100 * $data['amount'];
        $totalInterest = $interest * $data['term'];
        $total = $totalInterest + $data['amount'];

        $insert = [
            'identifier' => Str::uuid(),
            'balance' => $total,
            'remaining_term' => $data['mode'] * $data['term']
        ];

        return $this->loans()->create(array_merge((array) $insert, (array) $data));
    }

    public function path()
    {
        return "/dashboard/client/$this->identifier";
    }
}
