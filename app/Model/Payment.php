<?php

namespace App\Model;

use App\Model\Loan;
use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    protected $fillable = [
        'identifier', 'loan_id', 'amount', 'interest', 'principal'
    ];

    /**
     * Get the route key for the model.
     *
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'identifier';
    }

    public function loan()
    {
        return $this->belongsTo(Loan::class);
    }
}
