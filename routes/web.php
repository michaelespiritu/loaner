<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('home')->middleware(['verified', 'auth']);

Route::prefix('dashboard')->middleware(['verified', 'auth'])->group(function () {
    Route::resource('company', 'Web\CompanyController');
    Route::post('/company/{company}/invite', 'Web\CompanyController@invite')->name('company.invite');
    Route::post('/company/{company}/detach', 'Web\CompanyController@detach')->name('company.detach');

    Route::resource('client', 'Web\ClientController')->except([
        'store'
    ]);

    Route::post('/company/{company}/client', 'Web\ClientController@store')->name('client.store');

    Route::get('/client/{client}/loan', 'Web\LoanController@index')->name('loan.index');
    Route::get('/client/{client}/loan/create', 'Web\LoanController@create')->name('loan.create');
    Route::post('/client/{client}/loan/create', 'Web\LoanController@store')->name('loan.store');


    Route::get('/client/{client}/loan/{loan}', 'Web\LoanController@termsAndCondition')->name('loan.terms');


    Route::post('/loan/{loan}/payment', 'Web\LoanController@payment')->name('loan.payment');
    Route::patch('/loan/{loan}', 'Web\LoanController@update')->name('loan.update');

});

Auth::routes(['verify' => true]);

