<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model\Client;
use Illuminate\Support\Str;
use Faker\Generator as Faker;

$factory->define(Client::class, function (Faker $faker) {
    return [
        'identifier' => Str::uuid(),
        'name' => $faker->name,
        'email' => $faker->email,
        'address' => $faker->address,
        'birthday' => '25-05-1990',
    ];
});
