<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model\Loan;
use Illuminate\Support\Str;
use Faker\Generator as Faker;

$factory->define(Loan::class, function (Faker $faker) {
    return [
        'identifier' => Str::uuid(),
        'client_id' => factory(Client::class)->create(),
        'amount' => 6000,
        'interest' => 3,
        'term' => 4,
        'mode' => 2,
    ];
});
