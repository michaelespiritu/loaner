<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\User;
use App\Model\Company;
use Faker\Generator as Faker;

$factory->define(Company::class, function (Faker $faker) {
    return [
        'identifier' => Str::uuid(),
        'owner_id' => factory(User::class)->create(),
        'name' => $faker->company,
        'company_image' => ''
    ];
});
